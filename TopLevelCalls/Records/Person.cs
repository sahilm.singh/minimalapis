﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Records
{
    public class PersonClass
    {
        public string Name { get; set; }
        public DateOnly DOB { get; set; }

    }
    public record PersonRecord(string Name, DateOnly DOB)
    {
        protected virtual bool PrintMembers(StringBuilder builder)
        {
            builder.Append("test");
            return true;
        }
    }

    //public record PersonRecord
    //{
    //    public string Name { get; init; }
    //    public DateTime DOB { get; init; }
    //}
}
