﻿using Records;

var personRecord1 = new PersonRecord("Hiro Hamada", new DateOnly(1993,12,12));
var personRecord2 = new PersonRecord("Hiro Hamada", new DateOnly(1993, 12, 12));
var newPerson = personRecord1 with { DOB = new DateOnly(1993, 12, 2) };
var (name, dob) = personRecord1;

var personClass1 = new PersonClass
{
    Name = "Hiro Hamada",
    DOB = new DateOnly(1993, 12, 12)
};
var personClass2 = new PersonClass
{
    Name = "Hiro Hamada",
    DOB = new DateOnly(1993, 12, 12)
};

//Console.WriteLine(personClass1 == personClass2);
Console.WriteLine(personClass1);

//Console.WriteLine(personClass);
//Console.WriteLine(personRecord1);
