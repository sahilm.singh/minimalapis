﻿namespace TopLevelCalls
{
    public class Program1
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Add(4, 3));

            static double Add(int x, int y)
            {
                return x + y;
            }

        }

    }
}
