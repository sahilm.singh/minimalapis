﻿using Microsoft.EntityFrameworkCore;
using MinimalWebAPI.APIs;

namespace MinimalWebAPI.EndpointDefination
{
    public static class EndpointsDefiniation
    {
        public static void GetEndpointsDefiniations(this WebApplication app)
        {
            var apis = app.Services.GetServices<IAPI>();
            foreach(var api in apis)
            {
                if(api is null) throw new InvalidProgramException("API not found");
                api.Register(app);
            }   
        }

        public static void UseServices(this IServiceCollection services)
        {
            services.AddDbContext<ApplicationDb>(opt => opt.UseInMemoryDatabase("ApplicationList"));
            services.AddTransient<IAPI, CustomerAPIs>();
            services.AddTransient<IAPI, EmployeeAPIs>();
            services.AddDatabaseDeveloperPageExceptionFilter();

        }
    }
}
