using MinimalWebAPI.EndpointDefination;

var builder = WebApplication.CreateBuilder(args);
builder.Services.UseServices();

var app = builder.Build();
app.GetEndpointsDefiniations();
app.Run();


