﻿using Microsoft.EntityFrameworkCore;
using MinimalWebAPI.Model;

namespace MinimalWebAPI
{
    public class ApplicationDb : DbContext
    {
        public ApplicationDb(DbContextOptions<ApplicationDb> options)
            : base(options) { }

        public DbSet<Customer> Customers => Set<Customer>();
        public DbSet<Employee> Employees => Set<Employee>();
    }
}
