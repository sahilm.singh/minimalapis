﻿using Microsoft.EntityFrameworkCore;
using MinimalWebAPI.Model;

namespace MinimalWebAPI.APIs
{
    public class CustomerAPIs:IAPI
    {
        public void Register(WebApplication app)
        {
            
            app.MapGet("/", () => "Hello World!");
            app.MapGet("/Customers", GetAll);
            app.MapGet("/Customer/{id}", GetCustomerByID);
            app.MapPost("/Customer", AddCustomer);
            app.MapPut("/Customer/{id}", UpdateCustomer);
            app.MapDelete("/Customer/{id}", DeleteCustomer);
        }
        internal async Task<IResult> GetCustomerByID(int id, ApplicationDb db)
        {
            try
            {
                return await db.Customers.FindAsync(id)
                    is Customer Customer
                        ? Results.Ok(Customer)
                        : Results.NotFound();
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> DeleteCustomer(int id, ApplicationDb db)
        {
            try
            {
                if (await db.Customers.FindAsync(id) is Customer Customer)
                {
                    db.Customers.Remove(Customer);
                    await db.SaveChangesAsync();
                    return Results.Ok(Customer);
                }

                return Results.NotFound();
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> UpdateCustomer(int id, Customer inputCustomer, ApplicationDb db)
        {
            try
            {
                var Customer = await db.Customers.FindAsync(id);

                if (Customer is null) return Results.NotFound();

                Customer.Name = inputCustomer.Name;

                await db.SaveChangesAsync();

                return Results.NoContent();
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> AddCustomer(ApplicationDb db, Customer cust)
        {
            try
            {
                db.Customers.Add(cust);
                await db.SaveChangesAsync();

                return Results.Created($"/Customer/{cust.Id}", cust);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> GetAll(ApplicationDb db)
        {
            try
            {
                return Results.Ok(await db.Customers.OrderBy(x=>x.Id).ToListAsync());
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}


