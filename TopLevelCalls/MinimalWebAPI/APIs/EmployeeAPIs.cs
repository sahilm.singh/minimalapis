﻿using Microsoft.EntityFrameworkCore;
using MinimalWebAPI.Model;

namespace MinimalWebAPI.APIs
{
    public class EmployeeAPIs : IAPI
    {
        public void Register(WebApplication app)
        {

            app.MapGet("/Employees", GetAll);
            app.MapGet("/Employee/{id}", GetEmployeeByID);
            app.MapPost("/Employee", AddEmployee);
            app.MapPut("/Employee/{id}", UpdateEmployee);
            app.MapDelete("/Employee/{id}", DeleteEmployee);
        }
        internal async Task<IResult> GetEmployeeByID(int id, ApplicationDb db)
        {
            try
            {
                return await db.Employees.FindAsync(id)
                    is Employee Employee
                        ? Results.Ok(Employee)
                        : Results.NotFound();
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> DeleteEmployee(int id, ApplicationDb db)
        {
            try
            {
                if (await db.Employees.FindAsync(id) is Employee Employee)
                {
                    db.Employees.Remove(Employee);
                    await db.SaveChangesAsync();
                    return Results.Ok(Employee);
                }

                return Results.NotFound();
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> UpdateEmployee(int id, Employee inputEmployee, ApplicationDb db)
        {
            try
            {
                var Employee = await db.Employees.FindAsync(id);

                if (Employee is null) return Results.NotFound();

                Employee.Name = inputEmployee.Name;

                await db.SaveChangesAsync();

                return Results.NoContent();
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> AddEmployee(ApplicationDb db, Employee cust)
        {
            try
            {
                db.Employees.Add(cust);
                await db.SaveChangesAsync();

                return Results.Created($"/Employee/{cust.Id}", cust);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        internal async Task<IResult> GetAll(ApplicationDb db)
        {
            try
            {
                return Results.Ok(await db.Employees.OrderBy(x => x.Id).ToListAsync());
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}


