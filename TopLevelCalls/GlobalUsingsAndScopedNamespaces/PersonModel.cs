﻿namespace GlobalUsingsAndScopedNamespaces;

public class PersonModel
{
    public int Id { get; init; }
    public string Name { get; set; }
    public int Age { get; set; }

    public PersonModel()
    {

    }

    public PersonModel(int id, string name, int age)
    {
        Id = id;
        Name = name;
        Age = age;
    }
}

