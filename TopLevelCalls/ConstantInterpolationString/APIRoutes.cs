﻿namespace ConstantInterpolationString;

static class APIRoutes
{
    private const string APIRoot = "/api";

    private static class Library
    {
        private const string Base = APIRoot + "/lib";
        private const string Classes = $"{Base}/{{id:guid}}";
    }
}
