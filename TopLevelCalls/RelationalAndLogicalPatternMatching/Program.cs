﻿using RelationalAndLogicalPatternMatching;


var person1 = new PersonModel {Id = 1, Name ="Itachi", Age=22};
var person2 = new PersonModel { Id = 2, Name = "Minato", Age = 30 };
var person3 = new PersonModel { Id = 3, Name = "Jiraya", Age = 45 };

var persons = new List<PersonModel> { person1, person2, person3 };

foreach (var person in persons)
{ 
    switch (person.Age)
    {
        case 22:
            Console.WriteLine("young");
            break;
        case 30:
            Console.WriteLine("adult");
            break;
        case 45:
            Console.WriteLine("Old/Adult");
            break;

    }
}

//Relation Pattern >, <, >=, <=
//Logical Pattern and, or, not
foreach (var person in persons)
{
    person.Summary = person.Age switch
    { 
        < 30 => "young",
        >=30 and <=50 => "Adult",
        _ => "unknow"
    };
}





